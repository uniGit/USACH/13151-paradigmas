fibonacci(0,0).
fibonacci(1,1).

fibonacci(N,Resultado):-
    X is N-1, Y is N-2,
    fibonacci(X,Resultado1), fibonacci(Y,Resultado2),
    Resultado is Resultado1 + Resultado2.