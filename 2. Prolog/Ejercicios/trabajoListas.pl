primero([X|_],X).
resto([_|L],L).

/* Insertar un elemento E en la posición Pos de la lista L */
insertar([L|Ls],0, E, Out):- Out = [L|[E|Ls]].
insertar([L|Ls],Pos, E, Out):-
    PosSig is Pos -1,
    insertar(Ls, PosSig, E, OutAux),
    Out = [L|OutAux].

