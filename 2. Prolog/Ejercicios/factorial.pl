factorial(1,1).
factorial(X,Resultado):-
    X>1, Anterior is X-1,
    factorial(Anterior,ResultadoAnterior), Resultado is X*ResultadoAnterior.