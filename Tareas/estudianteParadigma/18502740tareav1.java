public class EstudianteParadigmas{
    String rut;
    String nombre;
    String[] notasCatedra;
    String[] notasLaboratorio;
    boolean estadoAprobacionCatedra;
    boolean estadoAprobacionLaboratorio;

    //Constructor
    public nuevoAlumno(String rut, String nombre){
        this.rut = rut;
        this.nombre = nombre;
        this.notasCatedra = new ArrayList<Double>(4);
        this.notasLaboratorio = new ArrayList<Double>(4);
    }

    //Métodos
    public void agregarNota(Double nota, String option){
        switch (option) {
            case 'catedra':
                this.notasCatedra.add(nota);
                break;
        
            case 'laboratorio':
                this.notasLaboratorio.add(nota);
                break;

            default:
                System.out.println('Opción inválida');
                break;
        }
    }

    public void verNotas(String option){
        if(option == 'catedra' || option == 'laboratorio'){
            System.out.println('-- Notas --');
            for(int i; i <= 4; i++){
                switch (option){
                    case 'catedra':
                        System.out.println('Catedra ' + i + ': ' + this.notasCatedra[i]);
                    case 'laboratorio':
                        System.out.println('Laboratorio ' + i + ': ' + this.notasLaboratorio[i]);
                }
            }
            System.out.println('-- Fin notas --');
        }
        else{
            System.out.println('Respuesta inválida');
        }
        switch (option) {
            case 'catedra':
                System.out.println('-- Notas Cátedra --');
                for(int i; i <= 4; i++){
                    System.out.println('Nota ' + i + ': ' + this.notasCatedra[i]);
                }
                System.out.println('-- Fin notas cátedra --');
                break;
        
            case 'laboratorio':
                System.out.println('-- Notas Laboratorio --');
                for(int i; i <= 4; i++){
                    System.out.println('Nota ' + i + ': ' + this.notasLaboratorio[i]);
                }
                System.out.println('-- Fin notas laboratorio --');
                break;

            default:
                System.out.println('Respuesta inválida');
                break;
        }
    }

    public void calcularEstado(String option){
        switch (option) {
            case 'catedra':
                System.out.println('-- Estado Cátedra --');

                // Primero, calculo del promedio
                double promedioCatedra = 0;
                for(int i; i <= 4; i++){
                    promedioCatedra += this.notasCatedra[i];
                }
                promedioCatedra = promedioCatedra/4;

                // Segundo, definir output según notas y promedio
                if((this.notasCatedra[0] && this.notasCatedra[1] && this.notasCatedra[2] && this.notasCatedra[3]) > 4){
                    estadoAprobacionCatedra = 1;
                    System.out.println('Estado Cátedra: Aprobado');
                }
                elseif (promedioCatedra >= 5){
                    estadoAprobacionCatedra = 1;
                    System.out.println('Estado Cátedra: Aprobado');
                }
                else{
                    System.out.println('Estado Cátedra: Pendiente del resultado en PA');
                }
                System.out.println('-- Fin estado cátedra --');
                break;
        
            case 'laboratorio':
                System.out.println('-- Estado Laboratorio --');

                //Si las 4 notas son sobre 4, se calcula el promedio
                if((this.notasLaboratorio[0] && this.notasLaboratorio[1] && this.notasLaboratorio[2] && this.notasLaboratorio[3]) > 4){
                    double promedioLaboratorio = 0;
                    for(int i; i <= 4; i++){
                        promedioLaboratorio += this.notasLaboratorio[i];
                    }
                    promedioLaboratorio = promedioLaboratorio/4;    
                }
                //Sino, se almacena el mínimo como promedio
                else{
                    double promedioLaboratorio = 4;
                    for(int i; i <= 4; i++){
                        if(promedioLaboratorio > this.notasLaboratorio[i]){
                            promedioLaboratorio = this.notasLaboratorio[i];
                        }
                    }
                }
                if(promedioLaboratorio > 4){
                    estadoAprobacionLaboratorio = 1;
                    System.out.println('Estado Laboratorio: Aprobado');
                }
                else{
                    estadoAprobacionLaboratorio = 0;
                    System.out.println('Estado Laboratorio: Reprobado');
                }
                System.out.println('-- Fin estado laboratorio --');
                break;

            default:
                System.out.println('Respuesta inválida');
                break;
        }
    }
}