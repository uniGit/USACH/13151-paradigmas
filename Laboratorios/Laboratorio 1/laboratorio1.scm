#lang racket
;Autor: V. Alex. Espinosa Rivera

(define (translate matrix1 matrix2)
  (map (lambda (lst) (map + lst matrix2)) matrix1)
)

(define (newlist n)
  (if (= n 1)
      (list 0)
      (cons 0(newlist (- n 1)))
   )
)

(define (matrix n m)
  (if (<= m 1)
      (list (newlist n))
      (cons (matrix n (- m 1)) (matrix n (- m 2)))
  )
)


