#lang racket

;Función para producir números aleatorios en un rango
(define (aleatorio max)
  (random (exact-round max))
)

;; Asteroides iniciales

(define (asteroidesiniciales n)
  (foreach)
      
      (define RandX (+ 1 (random 9)))
  (define RandY (+ 1 (random 9)))
   )
  )

(define (asteroide n)
  #t
  ;; Aquí va la función aleatorio
  ;; if (al azar se produce si es asteroide o no)
  ;;    0 -> asteroide -> ·
  ;;    1 -> espacio -> " "
  ;; Y esto debe generarse,para máximo 3 veces consecutivas
  )

; Produce listas vacías
(define (lista n)
  (if(= n 1)
     (cons " " null)
     (cons " " (lista (- n 1)))
     )
 )

; Produce la fila con la nave
(define (listanave n A)
  (if(= n 1)
     (cons " " null)
     (if (= n A)
         (cons "A" (listanave (- n 1) A))
         (cons " " (listanave (- n 1) A))
      )
     )
 )

; Manda a producir las filas
(define (matriz m n A)
  (if (= m 1)
      (cons (lista n) null)
      (if (= m A)
          (cons (listanave n (ceiling (/ n 2))) (matriz (- m 1) n A))
          (cons (lista n) (matriz (- m 1) n A))
       )
  )
 )

; Valida el ancho y el alto de la matriz
(define (validaralto m)
  (if (>= m 10)
      #t
      #f
   )
  )
(define (validarancho n)
  (if (>= n 5)
      #t
      #f
   )
 )

; Valida si la matriz es par o impar
(define (validarmatriz x)
      (if (= (modulo x 2) 0)
          #f
          #t
          )
 )

; Construye la matriz según la validación
; y considerando el valor medio
(define (construirmatriz n m)
  (if (and (validaralto m) (validarancho n))
  (if (validarmatriz m)
    (matriz m n (ceiling (/ m 2)))
    (matriz m n (/ m 2))
    )
   "Debe ser una matriz superior a 5 x 10"
  )
 )


;; Casos de Uso
;; Construir una matriz de m x n que ubica una nave
;; al medio de la matriz
;; (construirmatriz 10 10)
;; (construirmatriz 5 13)
;; (construirmatriz 20 12)